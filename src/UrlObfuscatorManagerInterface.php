<?php

namespace Drupal\url_obfuscator;

/**
 * Provides an interface defining the url obfuscator service.
 */
interface UrlObfuscatorManagerInterface {

  /**
   * The cipher method.
   *
   * For a list of available cipher methods, use openssl_get_cipher_methods().
   */
  const CIPHER_ALGO = 'AES-128-CBC';

  /**
   * Returns an url with an encrypted query string containing the parameters.
   *
   * @param string $route_path
   *   The path.
   * @param array $params
   *   An array of parameters to encrypt.
   * @param array $options
   *   See \Drupal\Core\Routing\UrlGeneratorInterface::generateFromRoute() for
   *   the available options.
   *
   * @return string
   *   The URL to the given route path containing an encrypted query string.
   */
  public function getEncryptedUrl(string $route_path, array $params = [], array $options = []);

  /**
   * Returns an encrypted query string containing the parameters.
   *
   * @param array $params
   *   An array of parameters to encrypt.
   *
   * @return string
   *   The encrypted query string.
   */
  public function getEncryptedQuery(array $params = []);

  /**
   * Returns a decrypted token.
   *
   * @param string $token
   *   The encrypted token.
   *
   * @return array
   *   The decrypted token.
   */
  public function getDecryptedQuery(string $token);

}
