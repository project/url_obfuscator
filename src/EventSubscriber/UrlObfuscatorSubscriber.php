<?php

namespace Drupal\url_obfuscator\EventSubscriber;

use Drupal\url_obfuscator\UrlObfuscatorManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Url obfuscator event subscriber.
 */
class UrlObfuscatorSubscriber implements EventSubscriberInterface {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The URL Obfuscator service.
   *
   * @var \Drupal\url_obfuscator\UrlObfuscatorManagerInterface
   */
  protected $urlObfuscator;

  /**
   * Constructs an UrlObfuscatorSubscriber object.
   *
   * @param \Drupal\url_obfuscator\UrlObfuscatorManagerInterface $url_obfuscator
   *   The url_obfuscator.manager service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(UrlObfuscatorManagerInterface $url_obfuscator, RequestStack $request_stack) {
    $this->urlObfuscator = $url_obfuscator;
    $this->requestStack = $request_stack;
  }

  /**
   * Kernel request event handler.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   Response event.
   */
  public function onKernelRequest(RequestEvent $event) {
    $request = $this->requestStack->getCurrentRequest();

    // We do not want the encrypted parameters to become visible,
    // so we do not propagate them via post.
    if ($request->getMethod() === 'POST') {
      return;
    }

    $encrypted_query = $request->get('oq');
    if (!empty($encrypted_query)) {
      $parameters = $this->urlObfuscator->getDecryptedQuery($encrypted_query);
      if (!empty($parameters)) {
        $request->query->add($parameters);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::REQUEST => ['onKernelRequest'],
    ];
  }

}
