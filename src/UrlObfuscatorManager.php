<?php

namespace Drupal\url_obfuscator;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Url;
use Drupal\key\KeyRepositoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * The (de)encrypt service for URLs and query strings.
 */
class UrlObfuscatorManager implements UrlObfuscatorManagerInterface {

  /**
   * The key.repository service.
   *
   * @var \Drupal\key\KeyRepositoryInterface
   */
  protected $keyRepository;

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The cached passphrase.
   *
   * @var string
   */
  protected $passphrase;

  /**
   * The cached initialization vector.
   *
   * @var string
   */
  protected $iv;

  /**
   * The module settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $settings;

  /**
   * Constructs an UrlObfuscatorManager object.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger channel factory.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\key\KeyRepositoryInterface $repository
   *   The key.repository service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(LoggerChannelFactoryInterface $logger, ConfigFactoryInterface $config_factory, KeyRepositoryInterface $repository, RequestStack $request_stack) {
    $this->settings = $config_factory->get('url_obfuscator.settings');
    $this->keyRepository = $repository;
    $this->logger = $logger;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public function getEncryptedUrl(string $route_path, array $params = [], array $options = []) {
    $query['oq'] = $this->getEncryptedQuery($params);
    if (!empty($options['query'])) {
      $options['query'] = array_merge($query, $options['query']);
    }
    $options += [
      'absolute' => TRUE,
    ];

    $path = '/' . trim($route_path, '\\/');

    return Url::fromUserInput($path, $options)->toString();
  }

  /**
   * {@inheritdoc}
   */
  public function getEncryptedQuery(array $params = []) {
    $data = [
      'params' => $params,
    ];

    return $this->encrypt($data);
  }

  /**
   * {@inheritdoc}
   */
  public function getDecryptedQuery($token) {
    $data = $this->decrypt($token);

    if (!empty($data['params'])) {
      return $data['params'];
    }
    else {
      // Prevent infinite loop by removing the oq query parameter.
      $this->requestStack->getCurrentRequest()->query->remove('oq');
      $this->getLogger()->notice('UrlObfuscator: Trying to access to an invalid obfuscated URL.');

      throw new NotFoundHttpException();
    }
  }

  /**
   * Decrypts a token.
   *
   * @param string $token
   *   The encrypted, BASE64 encoded and json encoded array token.
   *
   * @return array|false
   *   The decrypted token data.
   *   - params: Array of key/value pair.
   */
  protected function decrypt(string $token) {
    // Decode from BASE64.
    $token = $this->urlDecodeBase64($token);

    // Decrypt it.
    $decoded_token = openssl_decrypt($token, self::CIPHER_ALGO, $this->getSecretKey(), OPENSSL_RAW_DATA, $this->getIv());
    if ($decoded_token !== FALSE) {
      // Clean up the BOM and any stray null characters, to prevent a
      // JSON_ERROR_CTRL_CHAR error.
      // @see https://en.wikipedia.org/wiki/Byte_order_mark
      $decoded_token = trim($decoded_token, "\x0\xEF\xBB\xBF");

      // Decode the decrypted token from JSON.
      $decoded_token = Json::decode($decoded_token);
    }

    return $decoded_token;
  }

  /**
   * Generates an encrypted token from the given data.
   *
   * @param array $data
   *   The token data array.
   *   - params: Array of key/value pair.
   *
   * @return string
   *   A base64 string containing the json encoded and encrypted data.
   */
  protected function encrypt(array $data) {
    // Convert the array to json.
    $token = Json::encode($data);

    // Encrypt.
    $token = openssl_encrypt($token, self::CIPHER_ALGO, $this->getSecretKey(), OPENSSL_RAW_DATA, $this->getIv());

    // Encode it into BASE64.
    return $this->urlEncodeBase64($token);
  }

  /**
   * Returns a URL base64 encoded string.
   *
   * @param string $string
   *   The string to encode.
   *
   * @return string
   *   The encoded string.
   */
  protected function urlEncodeBase64(string $string) {
    return rtrim(
      strtr(base64_encode($string), '+/', '-_'),
    '='
    );
  }

  /**
   * Returns a URL base64 decoded string.
   *
   * @param string $string
   *   The encoded string.
   *
   * @return string
   *   The decoded string.
   */
  protected function urlDecodeBase64(string $string) {
    $string = str_pad(
      strtr($string, '-_', '+/'),
      strlen($string) % 4,
      '=',
      STR_PAD_RIGHT
    );

    return base64_decode($string);
  }

  /**
   * Returns the URL Obfuscator secret key.
   */
  protected function getSecretKey() {
    if (empty($this->passphrase)) {
      $this->passphrase = $this->keyRepository->getKey($this->settings->get('passphrase'))->getKeyValue();
    }

    return $this->passphrase;
  }

  /**
   * Returns a non-NULL initialization vector.
   */
  protected function getIv() {
    if (empty($this->iv)) {
      $this->iv = $this->keyRepository->getKey($this->settings->get('iv'))->getKeyValue();
    }

    return $this->iv;
  }

  /**
   * Provides the 'url_obfuscator' channel logger.
   *
   * @return \Psr\Log\LoggerInterface
   *   The 'url_obfuscator' channel logger.
   */
  protected function getLogger() {
    return $this->logger->get('url_obfuscator');
  }

}
