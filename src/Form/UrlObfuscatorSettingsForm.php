<?php

namespace Drupal\url_obfuscator\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure URL Obfuscator settings for this site.
 */
class UrlObfuscatorSettingsForm extends ConfigFormBase {

  /**
   * The key.repository service.
   *
   * @var \Drupal\key\KeyRepositoryInterface
   */
  protected $keyRepository;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->keyRepository = $container->get('key.repository');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'url_obfuscator_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['url_obfuscator.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->config('url_obfuscator.settings');

    $form['passphrase'] = [
      '#type' => 'key_select',
      '#title' => $this->t('Passphrase'),
      '#default_value' => $settings->get('passphrase'),
      '#key_filters' => [
        'type' => 'authentication',
      ],
    ];
    $form['iv'] = [
      '#type' => 'key_select',
      '#title' => $this->t('Initialization Vector'),
      '#description' => $this->t('It must be 16 characters long.'),
      '#default_value' => $settings->get('iv'),
      '#key_filters' => [
        'type' => 'authentication',
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $iv = $this->keyRepository->getKey($form_state->getValue('iv'))->getKeyValue();
    if (strlen($iv) != 16) {
      $form_state->setErrorByName('iv', $this->t('IV must be 16 characters long.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('url_obfuscator.settings')
      ->set('passphrase', $form_state->getValue('passphrase'))
      ->set('iv', $form_state->getValue('iv'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
