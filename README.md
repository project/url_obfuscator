URL Obfuscator
==============

This module provides the functionality to (de)encrypt URLs and query strings.

### Example

```php
<?php

  use Drupal\Core\Url;

  $params = [
    'id' => 'My_ID',
    'name' => 'John',
    'email' => 'example@example.com',
  ];
  $query = \Drupal::service('url_obfuscator.manager')->getEncryptedQuery($params);

  $url = Url::fromRoute('<front>', [], ['query' => $query])
```


### SPONSORS
- Initial development: [Fundación UNICEF Comité Español](https://www.unicef.es)
- Contrib version: [Amara NZero](https://amaranzero.com)

### CONTACT
Developed and maintained by Cambrico (http://cambrico.net).

Get in touch with us for customizations and consultancy:
http://cambrico.net/contact

#### Current maintainers:
- Pedro Cambra [(pcambra)](http://drupal.org/user/122101)
- Manuel Egío [(facine)](http://drupal.org/user/1169056)
